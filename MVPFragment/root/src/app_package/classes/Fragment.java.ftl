package ${packageName};

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ${rootPackageName}.R;
import ${rootPackageName}.core.MvpFragment;

import javax.inject.Inject;

public final class ${activityClass} extends MvpFragment<${presenterClass}> implements ${viewClass}
{
    
    public static ${activityClass} newInstance() {
        return new ${activityClass}();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.${layoutName}, container, false);
        setContentView(view);
        return view;
    }
}
