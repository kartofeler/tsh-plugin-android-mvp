package ${packageName};

import android.support.annotation.NonNull;
import ${rootPackageName}.core.MvpPresenter;

import javax.inject.Inject;

public final class ${presenterClass} extends MvpPresenter<${viewClass}>
{
    @Inject
    public ${presenterClass}(){}
}