package ${packageName};

import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import ${rootPackageName}.R;
import ${rootPackageName}.core.MvpActivity;

public final class ${activityClass} extends MvpActivity<${presenterClass}> implements ${viewClass}
{

	public static void startActivity(Context context) {
        Intent intent = new Intent(context, ${activityClass}.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        getActivityComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.${layoutName});
    }
}
