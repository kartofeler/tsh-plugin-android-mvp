# Templatki do Android MVP robione wg zamysłu Rafała

Jest na bazie [Taka tam biblioteczka](https://github.com/kartofeler/Android-Studio-MVP-template/tree/master/MVPActivity). Ma być pomocne - jeśli nie jest, napiszcie do mnie.


> Uwaga! Sprawdzone działanie tylko na Macbookach

Tworzysz nowy Fragment lub Activity i marzy Ci się od razu struktura MVP? Jak masz projekt z core'em Rafała to te templatki Ci pomogą!

```
com.example
    +-- ui.screenName
    |   - MainViewPresenter
    |   - MainActivity (lub) MainFragment
    |   - MainView
```

## Prerequisites

Fajnie jakby klasy bazowe z MVP od Rafała były w paczce .core. Inaczej wszystko szlag strzeli i se będziesz musiał ręcznie to zmienić.

## Installation

Wleź w klonowany/ściągnięty projekt i odpal komendę w bashu

```
sh ./install.sh
```

Uda się, jeśli masz Android Studio zaistalowane w domyślnym folderze, inaczej musisz zrobić 

Just copy all 3 directories `MVPFragment`, `MVPActivity` and `MVPBoilerplate` to `$ANDROID_STUDIO_FOLDER$/Contents/plugins/android/lib/templates/activities/`

Potem zrestartuj Android Studio. Włala

## License

    Copyright 2016 Benoit LETONDOR

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
